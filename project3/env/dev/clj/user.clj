(ns user
  (:require [mount.core :as mount]
            project3.core))

(defn start []
  (mount/start-without #'project3.core/repl-server))

(defn stop []
  (mount/stop-except #'project3.core/repl-server))

(defn restart []
  (stop)
  (start))


