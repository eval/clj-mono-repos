(ns project3.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [project3.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[project3 started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[project3 has shut down successfully]=-"))
   :middleware wrap-dev})
