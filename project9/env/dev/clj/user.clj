(ns user
  (:require [mount.core :as mount]
            project9.core))

(defn start []
  (mount/start-without #'project9.core/repl-server))

(defn stop []
  (mount/stop-except #'project9.core/repl-server))

(defn restart []
  (stop)
  (start))


