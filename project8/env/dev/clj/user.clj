(ns user
  (:require [mount.core :as mount]
            project8.core))

(defn start []
  (mount/start-without #'project8.core/repl-server))

(defn stop []
  (mount/stop-except #'project8.core/repl-server))

(defn restart []
  (stop)
  (start))


