(ns user
  (:require [mount.core :as mount]
            project5.core))

(defn start []
  (mount/start-without #'project5.core/repl-server))

(defn stop []
  (mount/stop-except #'project5.core/repl-server))

(defn restart []
  (stop)
  (start))


