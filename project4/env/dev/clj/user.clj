(ns user
  (:require [mount.core :as mount]
            project4.core))

(defn start []
  (mount/start-without #'project4.core/repl-server))

(defn stop []
  (mount/stop-except #'project4.core/repl-server))

(defn restart []
  (stop)
  (start))


