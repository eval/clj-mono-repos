(ns user
  (:require [mount.core :as mount]
            project2.core))

(defn start []
  (mount/start-without #'project2.core/repl-server))

(defn stop []
  (mount/stop-except #'project2.core/repl-server))

(defn restart []
  (stop)
  (start))


