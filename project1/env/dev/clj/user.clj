(ns user
  (:require [mount.core :as mount]
            project1.core))

(defn start []
  (mount/start-without #'project1.core/repl-server))

(defn stop []
  (mount/stop-except #'project1.core/repl-server))

(defn restart []
  (stop)
  (start))


