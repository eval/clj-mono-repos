(ns user
  (:require [mount.core :as mount]
            project7.core))

(defn start []
  (mount/start-without #'project7.core/repl-server))

(defn stop []
  (mount/stop-except #'project7.core/repl-server))

(defn restart []
  (stop)
  (start))


