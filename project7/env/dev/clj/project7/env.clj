(ns project7.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [project7.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[project7 started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[project7 has shut down successfully]=-"))
   :middleware wrap-dev})
