(ns project10.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[project10 started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[project10 has shut down successfully]=-"))
   :middleware identity})
