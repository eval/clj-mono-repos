(ns user
  (:require [mount.core :as mount]
            project10.core))

(defn start []
  (mount/start-without #'project10.core/repl-server))

(defn stop []
  (mount/stop-except #'project10.core/repl-server))

(defn restart []
  (stop)
  (start))


