(ns project6.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[project6 started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[project6 has shut down successfully]=-"))
   :middleware identity})
