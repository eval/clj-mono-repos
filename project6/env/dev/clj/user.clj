(ns user
  (:require [mount.core :as mount]
            project6.core))

(defn start []
  (mount/start-without #'project6.core/repl-server))

(defn stop []
  (mount/stop-except #'project6.core/repl-server))

(defn restart []
  (stop)
  (start))


